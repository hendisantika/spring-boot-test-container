package com.hendisantika;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-test-container-postgres
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/19/22
 * Time: 22:20
 * To change this template use File | Settings | File Templates.
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PersonRepositoryTests {

    @Autowired
    private PersonRepository personRepository;

    @Test
    void testFindAllReturnsName() {
        // This is defined in tc-initscript.sql
        List<Person> persons = personRepository.findAll();
        assertThat(persons.size()).isOne();
        assertThat(persons.get(0).getFirstName()).isEqualTo("Little");
        assertThat(persons.get(0).getLastName()).isEqualTo("Red");
    }
}
