package com.hendisantika;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-test-container-postgres
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/19/22
 * Time: 22:17
 * To change this template use File | Settings | File Templates.
 */
@RestResource
@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
}
